/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.fmocc.tpi_2019.historial.cars.historycars.acceso;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;
import ues.fmocc.ingenieria.tpi1352019.accesodatos.libreriadatostaller.PasoProceso;

/**
 *
 * @author kevin
 */
@RunWith(MockitoJUnitRunner.class)
public class PasoProcesoFacadeTest extends SessionBeanTest<PasoProceso> {

    public PasoProcesoFacadeTest() {
        super(PasoProceso.class);
        registrosEsperados.add(new PasoProceso(1, 1));
        registrosEsperados.add(new PasoProceso(2, 1));

    }
    
    @Mock
    EntityManager ema;
    @InjectMocks
    private PasoProcesoFacade cut;
    private PasoProceso pasoProceso = new PasoProceso(1, 1);
    private List<PasoProceso> registrosEsperados = new ArrayList<>();

    @Override
    protected FacadeGenerico getSessionBean() {
        return cut;
    }

    @Override
    protected PasoProceso getEntity() {
        return pasoProceso;
    }

    @Override
    protected EntityManager getEntityManager() {
        return ema;
    }

    @Override
    protected List<PasoProceso> getLista() {
        return registrosEsperados;
    }

}
